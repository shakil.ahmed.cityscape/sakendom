export const convertSecondsToOtherFormat = (time = 0) => {
  const weeks = parseInt(time / (7 * 24 * 3600));
  time = time % (7 * 24 * 3600);

  const days = parseInt(time / (24 * 3600));
  time = time % (24 * 3600);

  const hours = parseInt(time / 3600);
  time = time % 3600;
  
  const minutes = parseInt(time / 60);
  time = time % 60;

  const seconds = time;

  return ({ weeks, days, hours, minutes, seconds });
};