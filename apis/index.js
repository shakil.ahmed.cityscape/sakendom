/* eslint-disable no-console */
import express from 'express';
import healthAPIs from './health/routes.js';

const application = express();
const port = process.env.PORT || 5000;

// application.get('/', (request, response) => {
//   const ipAddress = request.headers['x-forwarded-for'] || request.socket.remoteAddress;

//   response.status(200).json({ message: 'Request accepted', ip: ipAddress });
// });

application.use('/api/v1/en/health', healthAPIs);

application.listen(port, () => {
  console.log(`Server started, server listening on port ${port}...`);
});
