import { convertSecondsToOtherFormat } from "../../common/utils/time.js";

const getHealth = (request, response) => {
  const uptime = convertSecondsToOtherFormat(Math.floor(process.uptime()));

  const systemStatus = {
    status: 'UP',
    uptime,
  }

  response.status(200).json(systemStatus);
};

export default getHealth;