import express from 'express';
import getHealth from './v1/getHealth.js';

const router = express.Router();

router.get('/', getHealth);

export default router;
